#include <iostream>
#include <cstdlib>
#include <string>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <sstream>
using namespace std;

const int SCREEN_WIDTH=640;
const int SCREEN_HEIGHT=400;
const int SCREEN_BPP=32;
const int TAILLE=6;


// -- ball --------------------------------------
// structure permettant de stocker tous les
// éléments concernant la balle
// Auteur : Aurélien SATGER
// ----------------------------------------------
struct ball
{
    int x;     // abscisse du centre de la balle
    int y;     // ordonnée du centre de la balle
    int mvt_x; // mouvement sur l'axe des abscisses
    int mvt_y; // mouvement sur l'axe des ordonnées
    SDL_Surface *balle;
    SDL_Rect lectureB;
};


// -- initBall ----------------------------------
// initialise la balle
// * paramètres entrées :
// - "b" : fait appel à la structure "balle"
// Auteur : Aurélien SATGER
// ----------------------------------------------
void initBall(ball &b)
{
    //position de la balle
    b.x=SCREEN_WIDTH/2;
    b.y=SCREEN_HEIGHT/2;

    //rectangle de lecture de la balle
    b.lectureB.x=0;
    b.lectureB.y=0;
    b.lectureB.w=6;
    b.lectureB.h=6;

    //mouvements de la balle
    b.mvt_x=2;
    b.mvt_y=2;
}


// -- Joueur ------------------------------------
// structure permettant de stocker tous les
// éléments concernant les deux joueurs
// Auteur : Aurélien SATGER
// ----------------------------------------------
struct Joueur
{
    int x;
    int y;
    SDL_Surface *joueur;
    SDL_Rect wall;
};


// -- initJoueur1 -------------------------------
// initialise la raquette du joueur 1
// * paramètres entrées :
// - "j1" : fait appel à la structure "Joueur"
// Auteur : Aurélien SATGER
// ----------------------------------------------
void initJoueur1 (Joueur &j1)
{
    j1.wall.x=50;
    j1.wall.y=SCREEN_HEIGHT/2-43/2;//permet de centrer la raquette joueur 2
    j1.wall.w=11;
    j1.wall.h=43;

}


// -- initJoueur2 -------------------------------
// initialise la raquette du joueur 2
// * paramètres entrées :
// - "j2" : fait appel à la structure "Joueur"
// Auteur : Aurélien SATGER
// ----------------------------------------------
void initJoueur2 (Joueur &j2)
{
    j2.wall.x=575;
    j2.wall.y=SCREEN_HEIGHT/2-43/2; //permet de centrer la raquette joueur 2
    j2.wall.w=11;
    j2.wall.h=43;
}


// -- Button ------------------------------------
// structure permettant de stocker tous les
// éléments concernant les deux boutons "play" et
// "quit" du menu
// Auteur : Aurélien SATGER
// ----------------------------------------------
struct Button
{
    int x;
    int y;
    SDL_Surface *boutons;
    SDL_Rect lectureButton;
    SDL_Rect lectureButtonYellow;
};


// -- initButton1 -------------------------------
// initialise le bouton "Quit"
// * paramètres entrées :
// - "butt1" : fait appel à la structure "Button"
// Auteur : Aurélien SATGER
// ----------------------------------------------
void initButton1(Button &butt1)
{
    butt1.x=370;
    butt1.y=250;

    butt1.lectureButton.x=0;
    butt1.lectureButton.y=0;
    butt1.lectureButton.w=175;
    butt1.lectureButton.h=80;

    butt1.lectureButtonYellow.x=200;
    butt1.lectureButtonYellow.y=0;
    butt1.lectureButtonYellow.w=175;
    butt1.lectureButtonYellow.h=80;
}


// -- initButton2 -------------------------------
// initialise le bouton "Play"
// * paramètres entrées :
// - "butt2" : fait appel à la structure "Button"
// Auteur : Aurélien SATGER
// ----------------------------------------------
void initButton2(Button &butt2)
{
    butt2.x=110;
    butt2.y=250;

    butt2.lectureButton.x=0;
    butt2.lectureButton.y=100;
    butt2.lectureButton.w=175;
    butt2.lectureButton.h=80;

    butt2.lectureButtonYellow.x=200;
    butt2.lectureButtonYellow.y=100;
    butt2.lectureButtonYellow.w=175;
    butt2.lectureButtonYellow.h=80;
}


// -- collision ---------------------------------
// permet de créer la collision entre les
// raquettes et la balle
// * paramètres entrées :
// - "a" : premier rectangle
// - "b" : second rectangle
// * paramètre de sortie :
// - "false" : les rectangles ne se touchent pas
// - "true" : les rectangles se touchent
// Auteur : IUT
// ----------------------------------------------
bool collision(SDL_Rect a, SDL_Rect b)
{
    int leftA, leftB;
    int rightA, rightB;
    int topA, topB;
    int bottomA, bottomB;

    leftA = a.x;
    rightA = a.x + a.w;
    topA = a.y;
    bottomA = a.y + a.h;

    leftB = b.x;
    rightB = b.x + b.w;
    topB = b.y;
    bottomB = b.y + b.h;

    if(bottomA <= topB)
        return false;
    if(topA >= bottomB)
        return false;
    if(rightA <= leftB)
        return false;
    if(leftA >= rightB)
        return false;

    return true;
}


// -- moveBall ----------------------------------
// permet la création de mouvement de la balle
// en tenant compte de la fonction collision
// * paramètres entrées :
// - "b" : fait appel à la structure "ball"
// - "joueur_01" : raquette du joueur1 appelée
// dans la fonction collision
// - "joueur_02" : raquette du joueur appélée
// dans la fonction collision
// Auteur : IUT
// Modifiée par Aurélien SATGER
// ----------------------------------------------
void moveBall(ball &b, SDL_Rect joueur_01, SDL_Rect joueur_02 )
{
    SDL_Rect tmp;

    b.x+=b.mvt_x;

    tmp.x=b.x-TAILLE/2;
    tmp.y=b.y-TAILLE/2;
    tmp.h=TAILLE;
    tmp.w=TAILLE;

    // Correction Mouvement Horizontal
    if(collision(tmp,joueur_01) || collision(tmp,joueur_02))
    {
        b.x-=b.mvt_x;
        b.mvt_x*=-1;
    }

    b.y+=b.mvt_y;

    tmp.x=b.x-TAILLE/2;
    tmp.y=b.y-TAILLE/2;

    // Correction Mouvement Vertical
    if((b.y+TAILLE/2>SCREEN_HEIGHT) || (b.y-TAILLE/2<0) || collision(tmp,joueur_01) || collision(tmp,joueur_02))
    {
        b.y-=b.mvt_y;
        b.mvt_y*=-1;
    }
}


// -- loadImage ---------------------------------
// chargement d'une image
// * paramètres entrées :
// - "filename" : nom de l'image
// * paramètre de sortie : SDL_Surface contenant
//   l'image.
// Auteur : IUT
// ----------------------------------------------
SDL_Surface *load_image( string filename )
{
    SDL_Surface* loadedImage = NULL;
    SDL_Surface* optimizedImage = NULL;
    loadedImage = IMG_Load( filename.c_str() );
    if( loadedImage != NULL )
    {
        optimizedImage = SDL_DisplayFormat( loadedImage );
        SDL_FreeSurface( loadedImage );
    }
    return optimizedImage;
}


// -- applySurface ------------------------------
// c'est le copier-coller d'une surface sur une
// autre : on colle le rectangle "clip" de "source"
// sur "destination" à partir de "x,y
// Auteur : IUT
// ----------------------------------------------
void applySurface(int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip)
{
    SDL_Rect offset;
    offset.x = x;
    offset.y = y;
    SDL_BlitSurface( source, clip, destination, &offset );
}


// -- loadImageWithColorKey ---------------------
// chargement d'une image
// * paramètres entrées :
// - "filename" : nom de l'image
// - "r,g,b"    : couleur de la ColorKey; cette
//   couleur devient transparente !
// * paramètre de sortie : SDL_Surface contenant
//   l'image.
// Auteur : IUT
// ----------------------------------------------
SDL_Surface *loadImageWithColorKey(string filename, int r, int g, int b)
{

    SDL_Surface* loadedImage = NULL;
    SDL_Surface* optimizedImage = NULL;
    loadedImage = IMG_Load( filename.c_str() );

    if( loadedImage != NULL )
    {
        optimizedImage = SDL_DisplayFormat( loadedImage );
        SDL_FreeSurface( loadedImage );
        if( optimizedImage != NULL )
        {
            Uint32 colorkey = SDL_MapRGB( optimizedImage->format, r, g, b );
            SDL_SetColorKey( optimizedImage, SDL_SRCCOLORKEY, colorkey );
        }
    }

    return optimizedImage;
}


// -- showBall ----------------------------------
// permet l'affichage de balle à l'écran
// * paramètres entrées :
// - "ma_balle" : appel à la structure "ball"
// - "screen" : surface sur laquelle le jeu
// s'affiche
// Auteur : Aurélien SATGER
// ----------------------------------------------
void showBall(ball ma_balle, SDL_Surface *screen)
{
    applySurface(ma_balle.x, ma_balle.y, ma_balle.balle, screen, NULL);
}


// -- showMessageScreen -------------------------
// permet d'afficher une chaine de caractère
// * paramètres entrées :
// - "message" : chaine de caractère contenant le
// message à afficher
// - "x" et "y" : position du message sur l'écran
// - "font" : charge la police
// - "fontSize" : taille de la police
// - "textColor" : couleur du texte
// - "screen" : surface sur laquelle s'applique
// le message
// Auteur : IUT
// ----------------------------------------------
void showMessageScreen(string message,int x,int y, TTF_Font *font,int fontSize,SDL_Color textColor,SDL_Surface* &screen)
{
    string mot="";
    string space=" ";
    size_t i=0,j;
    SDL_Surface *mes=NULL;

    j = message.find(space);
    while( j != string::npos )
    {
        mot=message.substr(i,j-i);
        if(mot != "")
        {
            mes=TTF_RenderText_Solid(font,mot.c_str(),textColor);
            applySurface(x,y,mes,screen,NULL);
            x+=mes->w;
            SDL_FreeSurface(mes);
        }
        x+=fontSize;
        i=j+1;
        j = message.find(space,i);
    }

    mot=message.substr(i);
    mes=TTF_RenderText_Solid(font,mot.c_str(),textColor);
    applySurface(x,y,mes,screen,NULL);
    SDL_FreeSurface(mes);
}

// -- image -------------------------------------
// permet de charger les images sur le disque dur
// * paramètres entrées :prend en paramètre
// totues les surface qui vont venir s'appliquer
// sur la surface screen
// Auteur : Aurélien SATGER
// ----------------------------------------------
void image(SDL_Surface *&fond, SDL_Surface *&menu, ball &ma_balle, Joueur &joueur_01, Joueur &joueur_02, Button &butt)
{
    fond=load_image("fond.bmp");
    ma_balle.balle=loadImageWithColorKey("ball.bmp",0,255,255);
    joueur_01.joueur=loadImageWithColorKey("player01.bmp",0,255,255);
    joueur_02.joueur=loadImageWithColorKey("player02.bmp",0,255,255);
    menu=load_image("title.bmp");
    butt.boutons=loadImageWithColorKey("button.bmp",0,0,0);
}


// -- clean -------------------------------------
// permet de libérer la mémoire
// * paramètres entrées : prend en paramètre
// toutes les surfaces utilisées dans la scène
// Auteur : Aurélien SATGER
// ----------------------------------------------
void clean(SDL_Surface *&screen, SDL_Surface *&fond, SDL_Surface *&menu,Joueur &joueur_01, Joueur &joueur_02, Button &butt, ball &ma_balle)
{
    SDL_FreeSurface(screen);
    SDL_FreeSurface(fond);
    SDL_FreeSurface(joueur_01.joueur);
    SDL_FreeSurface(joueur_02.joueur);
    SDL_FreeSurface(butt.boutons);
    SDL_FreeSurface(menu);
    SDL_FreeSurface(ma_balle.balle);
}

// -- affichBouton -------------------------------------
// permet d'afficher la couleur des boutons du menu
// * paramètres entrées :
// - SDL_event pour la gestion des touches
// - Button butt/butt1/butt2 pour les boutons "play" et "quit"
// de couleur rouge et jaune
// - SDL_Surface *screen pour afficher les bouons sur la
// fenêtre de jeu
// Auteur : Aurélien SATGER
// ----------------------------------------------
void affichBouton (SDL_Event event, Button butt, Button butt1, Button butt2, SDL_Surface *screen)
{
    //initialisation des coordonnées de la souris
    int x = event.button.x;
    int y = event.button.y;

    //si la souris passe sur le bouton "play", on applique la surface du bouton jaune
    if( ( x > butt2.x ) && ( x < butt2.x + butt2.lectureButton.w ) && ( y > butt2.y ) && ( y < butt2.y + butt2.lectureButton.h ) )
    {
        applySurface(butt2.x,butt2.y,butt.boutons, screen,&butt2.lectureButtonYellow);
    }
    //sinon, le bouton est rouge
    else
    {
        applySurface(butt2.x,butt2.y,butt.boutons, screen,&butt2.lectureButton);

    }
    //si la souris passe sur le bouton "quit", on applique la surface du bouton jaune
    if( ( x > butt1.x ) && ( x < butt1.x + butt1.lectureButton.w ) && ( y > butt1.y ) && ( y < butt1.y + butt1.lectureButton.h ) )
    {
        applySurface(butt1.x,butt1.y,butt.boutons, screen,&butt1.lectureButtonYellow);
    }
    //sinon, le bouton est rouge
    else
    {
        applySurface(butt1.x,butt1.y,butt.boutons, screen,&butt1.lectureButton);
    }
}

int main()
{
    bool quit=false;
    int jeu;
    jeu = 0;

    SDL_Surface *screen;
    SDL_Surface *fond;
    SDL_Surface *menu;
    SDL_Event event;

    ball ma_balle;
    initBall(ma_balle);

    Joueur joueur_01, joueur_02;
    initJoueur1(joueur_01);
    initJoueur2(joueur_02);

    Button butt, butt1, butt2;
    initButton1(butt1);
    initButton2(butt2);

    //intialisation pour la gestion de l’affichage des fontes
    TTF_Init();
    //déclaration d'une variable de type fonte
    TTF_Font *fonts;
    //déclaration d'une variable permettant de créer la couleur du texte
    SDL_Color textColor;
    textColor.r=255;
    textColor.g=255;
    textColor.b=255;
    int fontSize=42;
    //déclaration du score du joueur 1 et du joueur 2
    int pointsJoueur1=0;
    int pointsJoueur2=0;

    //déclaration de la police
    string police="Hawaii_Killer.ttf";
    //ouverture de la fonte
    fonts = TTF_OpenFont(police.c_str(),fontSize);
    //déclaration des messages à afficher sur l'écran de jeu
    ostringstream mssg;
    ostringstream mssg2;

    SDL_Init(SDL_INIT_EVERYTHING);
    screen=SDL_SetVideoMode(SCREEN_WIDTH,SCREEN_HEIGHT, SCREEN_BPP,SDL_SWSURFACE);

    image(fond, menu, ma_balle, joueur_01, joueur_02, butt);

    while(!quit)
    {
        while (jeu == 1 && !quit) //boucle de jeu
        {

            applySurface(0,0,fond,screen,NULL);
            showBall(ma_balle, screen);
            applySurface(joueur_01.wall.x, joueur_01.wall.y, joueur_01.joueur, screen, NULL);
            applySurface(joueur_02.wall.x, joueur_02.wall.y, joueur_02.joueur, screen, NULL);

            //affiche le score du joueur1 initialisé à 0
            mssg.flush();
            mssg.str("");
            mssg <<  pointsJoueur1;

            if ((ma_balle.x+TAILLE/2) > SCREEN_WIDTH )
            {
                pointsJoueur1 ++;
                //initialise la balle au centre de l'écran
                ma_balle.x=SCREEN_WIDTH/2;
                ma_balle.y=SCREEN_HEIGHT/2;
                mssg.flush();
                mssg.str("");
                mssg <<  pointsJoueur1;
            }

            showMessageScreen(mssg.str(),50,0,fonts,fontSize,textColor,screen);

            //affiche le score du joueur2 initialisé à 0
            mssg2.flush();
            mssg2.str("");
            mssg2 <<  pointsJoueur2;

            if ((ma_balle.x-TAILLE/2)< 0)
            {
                pointsJoueur2 ++;
                //initialise la balle au centre de l'écran
                ma_balle.x=SCREEN_WIDTH/2;
                ma_balle.y=SCREEN_HEIGHT/2;
                mssg2.flush();
                mssg2.str("");
                mssg2 <<  pointsJoueur2;
            }

            showMessageScreen(mssg2.str(),575,0,fonts,fontSize,textColor,screen);

            SDL_Flip(screen);

            while(SDL_PollEvent(&event))
            {
                if( event.type == SDL_QUIT )
                {
                    quit = true;
                }
                //Lorsqu'une touche est pressée
                else if( event.type == SDL_KEYDOWN )
                {
                    //associons des différentes touches pour les mouvements des deux raquettes
                    switch( event.key.keysym.sym )
                    {
                    //fleche haut
                    case SDLK_UP: joueur_02.wall.y-=1; break;
                    //fleche bas
                    case SDLK_DOWN: joueur_02.wall.y+=1; break;
                    case SDLK_z: joueur_01.wall.y-=1; break;
                    case SDLK_s: joueur_01.wall.y+=1; break;
                    case SDLK_ESCAPE: jeu = 0; break;
                    default: break;
                    }
                }
            }
            Uint8 *keystates = SDL_GetKeyState( NULL );

            //lorsque qu'on reste appuyé sur "haut"
            if( keystates[ SDLK_UP ] )
            {
                joueur_02.wall.y-=3;

                //permet de bloquer la raquette 2 en haut de la fenêtre
                if (joueur_02.wall.y<=0)
                {
                    joueur_02.wall.y=0;
                }
            }

            //lorsque qu'on reste appuyé sur "bas"
            if( keystates[ SDLK_DOWN ] )
            {
                joueur_02.wall.y+=3;

                //permet de bloquer la raquette 2 en bas de la fenêtre
                if (joueur_02.wall.y >= 357)
                {
                    joueur_02.wall.y=357;
                }
            }
            //lorsque qu'on reste appuyé sur "z"
            if( keystates[ SDLK_z ] )
            {
                joueur_01.wall.y-=3;

                //permet de bloquer la raquette 1 en haut de la fenêtre
                if (joueur_01.wall.y<=0)
                {
                    joueur_01.wall.y=0;
                }
            }

            //lorsque qu'on reste appuyé sur "s"
            if( keystates[ SDLK_s ] )
            {
                joueur_01.wall.y+=3;

                //permet de bloquer la raquette 1 en bas de la fenêtre
                if (joueur_01.wall.y >=357)
                {
                    joueur_01.wall.y=357;
                }
            }

            moveBall(ma_balle,joueur_01.wall, joueur_02.wall);
            SDL_Delay(10);
        }

        //permet de quitter le jeu
        if (jeu == -1)
        {
            quit = true;
        }

        //boucle du menu
        while (jeu == 0 && !quit) //boucle de menu
        {
            //initialisation du score et de la balle
            pointsJoueur1=0;
            pointsJoueur2=0;
            ma_balle.x=SCREEN_WIDTH/2;
            ma_balle.y=SCREEN_HEIGHT/2;
            joueur_01.wall.x=50;
            joueur_01.wall.y=SCREEN_HEIGHT/2-43/2;
            joueur_02.wall.x=575;
            joueur_02.wall.y=SCREEN_HEIGHT/2-43/2;

            //permet d'interagir avec un élément lorque la souris passe dessus
            if( event.type == SDL_MOUSEMOTION)
            {
                affichBouton(event, butt, butt1, butt2, screen);
            }
            else
            {
                applySurface(0,0,menu,screen,NULL);
                applySurface(butt1.x,butt1.y,butt.boutons, screen,&butt1.lectureButton);
                applySurface(butt2.x,butt2.y,butt.boutons, screen,&butt2.lectureButton);
            }
            SDL_Flip(screen);

            while(SDL_PollEvent(&event))
            {
                if( event.type == SDL_QUIT )
                {
                    quit = true;
                }
                if( event.type == SDL_MOUSEBUTTONDOWN )
                {
                    //les coordonnées de la souris
                    int x = event.button.x;
                    int y = event.button.y;

                    //bouton "Play"
                    //lorsqu'un clic est efféctué sur le bouton deux le jeu se lance
                    if( ( x > butt2.x ) && ( x < butt2.x + butt2.lectureButton.w ) && ( y > butt2.y ) && ( y < butt2.y + butt2.lectureButton.h ) )
                    {
                        if (SDL_BUTTON_LEFT)
                        {
                            jeu = 1; //lance la boucle de jeu
                        }
                    }

                    //bouton "Quit"
                    //lorqu'un clic est éfféctué sur le bouton 1 le jeu quitte
                    if( ( x > butt1.x ) && ( x < butt1.x + butt1.lectureButton.w ) && ( y > butt1.y ) && ( y < butt1.y + butt1.lectureButton.h ) )
                    {
                        if (SDL_BUTTON_LEFT)
                        {
                            jeu = -1;//quitte le jeu
                        }
                    }
                }

            }
        }
    }
    //libération de la mémoire
    clean(screen,fond,menu, joueur_01, joueur_02, butt, ma_balle);
    SDL_Quit();
    return EXIT_SUCCESS;
}
